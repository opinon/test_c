#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include "commons.h"


int main(int argc, char** argv) {
	printf("Serveur started: %d\n", getpid()); 
	
	char pipe_name[20];
	sprintf(pipe_name,"pipe_%d", getpid());
	mkfifo(pipe_name, 0644);
	int pipe = open(pipe_name, O_RDWR);

	printf("Waiting ...\n");

	rcs* rcv_rcs = NULL;
	read(pipe, rcv_rcs, sizeof(rcs));
	printf("Got a connexion!\n");
	printf("Expr is : %s\n", rcv_rcs->expr);
	printf("Should have written expr\n");

	close(pipe);
	unlink(pipe_name);


	return 0;
}
