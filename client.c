#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include "commons.h"

int main(int argc, char** argv) {

	if(argc != 3) {
		printf("Usage: <pid serveur> <expr>\n");
		return 1;
	}	

	rcs my_rcs = {
		getpid(),
		argv[2],
	};

	printf("Sending %d %s\n", my_rcs.pidClient, my_rcs.expr);

	char pipe_server[20];
	sprintf(pipe_server, "pipe_%s", argv[1]);
	int df_server = open(pipe_server, O_WRONLY);

	write(df_server, &my_rcs, sizeof(rcs));
	

	return 0;
}
